$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 2000
    });
    $('#contacto').on('show.bs.modal', function (e){
        console.log('se está mostrando el modal');
         $('#contactoBtn').removeClass('btn-outline-success');
         $('#contactoBtn').addClass('btn-primary');		
         $('#contactoBtn').prop('disabled', true);			
    });
    $('#contacto').on('shown.bs.modal', function (e){
        console.log('ya se mostró el modal');			
    });
    $('#contacto').on('hidden.bs.modal', function (e){
        console.log('se cerró u ocultó el modal');	
        $('#contactoBtn').removeClass('btn-primary');
         $('#contactoBtn').addClass('btn-outline-success');	
        $('#contactoBtn').prop('disabled', false);		
            
    });
});